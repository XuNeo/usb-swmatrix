#include "stdio.h"
#include "app/res_array.h"

int main(void)
{
  volatile static int i=0;
  uint32_t res_count;
  float const *pres_table;

  printf("Hello from stm32f070\n");
  res_array_init();
  res_count = res_get_values(&pres_table);
  for(i=0;i<res_count;i++)
  {
    printf("res[%d]=%f\n", i, pres_table[i]);
  }
  while (1)
  {
    i++;
    if(i==100000)
    {
      i = 0;
      printf("Hello from stm32f070\n");
    }
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
