#include "stdio.h"
#include "stm32f0xx.h"

static char is_uart_inited;

#define DEFAULT_BAUDRATE 115200
static void __usart_init(uint32_t baudrate)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  //tx-->pa2, rx-->pa3
  USART_InitTypeDef USART_InitStructure;
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);
  /* Configure USART Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin =GPIO_Pin_3;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  /* USARTx configured as follow:
  - BaudRate = 115200 baud  
  - Word Length = 8 Bits
  - Stop Bit = 1 Stop Bit
  - Parity = No Parity
  - Hardware flow control disabled (RTS and CTS signals)
  - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = baudrate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART2, &USART_InitStructure);
  USART_Cmd(USART2, ENABLE);
}

// #ifdef __GNUC__
//   /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
//      set to 'Yes') calls __io_putchar() */
//   #define PUTCHAR_PROTOTYPE int _write(int file, char *ptr, int len)
// #else
//   #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
// #endif /* __GNUC__ */
#ifdef __GNUC__
int _write(int file, char *ptr, int len)
{
  int count = 0;
  if(is_uart_inited == 0)
  {
    is_uart_inited = 1;
    __usart_init(DEFAULT_BAUDRATE);
  }
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
  while(len--)
  {
    USART_SendData(USART2, (uint8_t) *ptr++);

    /* Loop until transmit data register is empty */
    while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET)
    {}
    count++;
  }

  return count;
}
#endif