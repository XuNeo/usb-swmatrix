#include "relays.h"
#include "stm32f0xx.h"

#define RELAY_CS_L()  GPIOA->BRR = GPIO_Pin_4
#define RELAY_CS_H()  GPIOA->BSRR = GPIO_Pin_4

static uint32_t relay_curr_sta; /**< Current relay settings status. */

/**
 * @brief init relay related hardware including GPIO and SPI.
 * @return none.
*/
void relay_hardware_init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef  SPI_InitStructure;

  //PA4-->CS PA5-->SCLK, PA7-->MOSI
  /* Enable the SPI periph */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

  /* Enable SCK, MOSI, MISO and NSS GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_0);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_0);
  //GPIO_PinAFConfig(SPIx_MISO_GPIO_PORT, SPIx_MISO_SOURCE, SPIx_MISO_AF);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
  /* SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* SPI  MOSI pin configuration */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* SPI MISO pin configuration */
  //GPIO_InitStructure.GPIO_Pin = SPIx_MISO_PIN;
  //GPIO_Init(SPIx_MISO_GPIO_PORT, &GPIO_InitStructure);

  /* CS pin */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  RELAY_CS_H();
  /* SPI configuration -------------------------------------------------------*/
  SPI_I2S_DeInit(SPI1);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;

  SPI_Init(SPI1, &SPI_InitStructure);
  SPI_Cmd(SPI1, ENABLE);
  relay_set(0); /* Disable all relays */
}

/**
 * @brief Set relay status. This function will affect all relays.
 * @param relay_sets: relay settings
 * @return none.
*/
void relay_set(uint32_t relay_bit_set)
{
  RELAY_CS_L();
  SPI_I2S_SendData16(SPI1, (uint16_t)relay_bit_set);
  //wait until all bits sent out.
  while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
  SPI_I2S_ReceiveData16(SPI1);  /* read to clr flag */
  RELAY_CS_H();
  relay_curr_sta = relay_bit_set;
}

/**
 * @brief Turn on selected relay(s).
 * @param relay_sets: the selected relays are going to be turned on.
 * @return none.
*/
void relay_set_bits(uint32_t relay_sets)
{
  uint32_t temp;
  temp = relay_read_set();
  temp |= relay_sets;
  relay_set(temp);
}

/**
 * @brief Turn off selected relay(s).
 * @param relay_sets: the selected relays are going to be turned off.
 * @return none.
*/
void relay_clr_bits(uint32_t relay_sets)
{
  uint32_t temp;
  temp = relay_read_set();
  temp &=~relay_sets;
  relay_set(temp);
}

/**
 * @brief get curret relay status.
 * @return none.
*/
uint32_t relay_read_set(void)
{
  return relay_curr_sta;
}
