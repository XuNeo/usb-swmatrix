#ifndef _RELAYS_H_
#define _RELAYS_H_
#include "stm32f0xx.h"
#define RELAY_1   0x0001
#define RELAY_2   0x0002
#define RELAY_3   0x0004
#define RELAY_4   0x0008
#define RELAY_5   0x0010
#define RELAY_6   0x0020
#define RELAY_7   0x0040
#define RELAY_8   0x0080
#define RELAY_9   0x0100
#define RELAY_10  0x0200
#define RELAY_11  0x0400
#define RELAY_12  0x0800
#define RELAY_13  0x1000
#define RELAY_14  0x2000
#define RELAY_15  0x4000
#define RELAY_16  0x8000

void relay_hardware_init(void);
void relay_set(uint32_t relay_bit_set); /* Set all relay status */
void relay_set_bits(uint32_t relay_sets);
void relay_clr_bits(uint32_t relay_sets);
uint32_t relay_read_set(void);      /* Read current settings */
#endif
