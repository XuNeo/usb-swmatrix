#ifndef _RES_ARRAY_H_
#define _RES_ARRAY_H_
#include "stm32f0xx.h"

void res_array_init(void);
uint32_t res_get_values(float const**pptable);
float res_set_index(uint32_t res_index);
float res_set_value(float value);

#endif
