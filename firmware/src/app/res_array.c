#include "res_array.h"
#include "bsp/relays.h"

#define RES_ARRAY_SIZE 16 /* How many resitors are in this array */
/**
 * resitors are in serial and relays connect the output to any one of the resistor end.
 * 
 * ---Res1---Res2---Res3---Res4---Res5---...---
 * |       |      |      |      |      |
 * |     Relay1 Relay2 Relay3 Relay4 Relay5...
 * |       |______|______|______|______|___
 * |<-----OUT------->|    
*/

/**< The resisors are in serail, the table is exact value of all resistors.  */
static float resitor_value_talbe[RES_ARRAY_SIZE]=
{
  10.0f,
  10.0f,
  49.9f,
  200.0f,
  200.0f,
  1000.0f,
  3000.0f,
  3000.0f,
  10000.0f,
  20000.0f,
  49900.0f,
  100000.0f,
  300000.0f,
  300000.0f,
  1000000.0f,
  1000000.0f,
};

/* The resistor value that this array can provided. */
static float resistor_out_table[RES_ARRAY_SIZE]=
{
  10.0f,
  10.0f+ 10.0f,
  10.0f+ 10.0f + 49.9f,
  10.0f+ 10.0f + 49.9f + 200.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f + 100000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f + 100000.0f + 300000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f + 100000.0f + 300000.0f + 300000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f + 100000.0f + 300000.0f + 300000.0f + 1000000.0f,
  10.0f+ 10.0f + 49.9f + 200.0f + 200.0f + 1000.0f + 3000.0f + 3000.0f + 10000.0f + 20000.0f + 49900.0f + 100000.0f + 300000.0f + 300000.0f + 1000000.0f + 1000000.0f,
};

void res_array_init(void)
{
  uint32_t i;
  float sum = 0;
  //build resistor output table
  for(i=0;i<RES_ARRAY_SIZE;i++)
  {
    sum += resitor_value_talbe[i];
    resistor_out_table[i] = sum;  //the output is sum of first resistor to the positon where relay are closed.
  }
  //hardware init
  relay_hardware_init();
}

/**
 * @brief get available resistor values that can be set.
 * @param ptable: the pointer that points to resistor array table.
 * @return return how many resistor values are available.
*/
uint32_t res_get_values(float const**pptable)
{
  *pptable = resistor_out_table;
  return RES_ARRAY_SIZE;
}

/**
 * @brief Set the resistor array by choosing a value close to provided resistor value.
 * @param value: the resistor value desired.
 * @return return the real resisor value.
*/
float res_set_value(float value)
{
  uint32_t index = 0;
  if(value <= resistor_out_table[0])
    ;
  else if(value >= resistor_out_table[RES_ARRAY_SIZE-1])
    index = RES_ARRAY_SIZE - 1;
  else
  for(;index<RES_ARRAY_SIZE-2;index++)
  {
    if(value > resistor_out_table[index])
      continue;
    else if(value <= resistor_out_table[index+1])
    {
      /* got you ! */
      if((value-resistor_out_table[index]) > \
          (resistor_out_table[index+1] - value))
      {
        //the next resistor is better.
        index ++;
      }
      break;
    }
  }
  return res_set_index(index);
}

/**
 * @brief Set the resistor by index value. the index is used to set the related relay directly.
 * @param res_index: the resistor index value. range is 0 to RES_ARRAY_SIZE
 * @return return noone.
*/
float res_set_index(uint32_t res_index)
{
  //the index is from 0 to RES_ARRAY_SIZE, the relay settings are bit masks
  uint32_t bit_mask;
  
  if(res_index < RES_ARRAY_SIZE)
  {
    bit_mask = 1<<res_index;
    relay_set(bit_mask);
    return resistor_out_table[res_index];
  }

  return 0;
}
